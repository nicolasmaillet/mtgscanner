import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import React, { FunctionComponent } from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';

import { AppStackParamList } from '../AppNavigator';

type Navigation = NativeStackNavigationProp<AppStackParamList, 'SampleOne'>;

const SampleScreenTwo: FunctionComponent = () => {
  const navigation = useNavigation<Navigation>();

  return (
    <View style={styles.container}>
      <Text>Sample Screen 2</Text>
      <Pressable onPress={() => navigation.navigate('SampleOne')}>
        <Text>go to 1</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

export default SampleScreenTwo;
